import os, sys
__dir__ = os.path.dirname(os.path.abspath(__file__))
sys.path.append( __dir__ + '/..')

from unittest import main, TestCase
from redis import StrictRedis
import worker.main
import worker.responder
import worker.parser

class TestWorker(TestCase):

    def setUp(self):
        self.client = StrictRedis(db=9)
        self.client.flushdb()
        with open(__dir__ + '/email0.txt') as f:
            email = f.read()
        self.client.rpush('inbox', email)
        self.command = worker.main.wait_for_command(self.client)

    def tearDown(self):
        self.client.flushdb()
        self.client.connection_pool.disconnect()

    def test_parser(self):
        self.assertEqual(self.command.sender, 'Vinay Pulim <v@pulim.com>')
        self.assertEqual(self.command.to, 'Lisa <lisa@cclisa.com>')
        self.assertEqual(self.command.subject, 'test0')

    def test_responder(self):
        response = worker.responder.respond(self.command)
        self.assertEqual(response['Subject'], 'Re: test0')
        self.assertEqual(response['From'], 'Lisa <lisa@cclisa.com>')
        self.assertEqual(response['To'], 'Vinay Pulim <v@pulim.com>')

class TestParser(TestCase):

    def test_parser(self):
        with open(__dir__ + '/parser_tests.txt') as f:
            while True:
                line = f.readline()
                if not line: break
                if line[0] == ';' or line[0] == '\n': continue
                line = line.strip()
                when = f.readline().strip()
                what = f.readline().strip()
                tags = f.readline().strip()
                parsed = worker.parser.ParsedText(line)
                self.assertEqual(when, parsed.when)
                self.assertEqual(what, parsed.what)
                self.assertEqual(tags, parsed.tags)


if __name__ == '__main__':
    main()

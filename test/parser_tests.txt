2012-01-01 00:00:00
Remind me to wake up at 7:15am
    remind
    wake up
    07:15:00

Reminder: Get Paul to school by 7:45am
    remind
    Get Paul to school
    07:45:00

Remember Sales Forecast Meeting Wednesday 8:30am
    remind
    Sales Forecast Meeting
    2012-01-04 08:30:00

remind me about Psych 101 mid-term next wed at 11:30pm
    remind
    Psych 101 mid-term
    2012-01-04 23:30:00

Reminder: Lunch meeting with Frank Sanchez Wed at noon
    remind
    Lunch meeting with Frank Sanchez
    2012-01-04 12:00:00

Remember to watch Paul's soccer practice on Wednesday 3-4:20 pm #weekly
    remind
    watch Paul's soccer practice
    2012-01-04 15:00:00 to 2012-01-04 16:20:00
    #weekly
Remember soccer practice after school Wed 3:30pm #weekly #paul #"project x"
    remind
    soccer practice after school
    2012-01-04 15:30:00
    #weekly #paul #"project x"
Remind me to refill my prescription tomorrow #todo
    remind
    refill my prescription
    2012-01-02
    #todo
Remember to put the porch light on a timer #travel, #Hawaii, #todo
    remind
    put the porch light on a timer

    #travel, #Hawaii, #todo
Remind us to call Mom on her birthday on 9/16/07 #yearly #birthday
    remind
    call Mom on her birthday
    2007-09-16
    #yearly #birthday
Remember Sam's Tae Kwon Do belt test from 10am to 2pm on 6/1
    remind
    Sam's Tae Kwon Do belt test
    2012-06-01 10:00:00 to 2012-06-01 14:00:00

Remember the plumber's number is (415) 555-1212
    remind
    the plumber's number is (415) 555-1212
    

Remember Pat's wife's name is Christine
    remind
    Pat's wife's name is Christine
    

Reminder Staff meeting next Tuesday 10-11am #meeting
    remind
    Staff meeting
    2012-01-03 10:00:00 to 2012-01-03 11:00:00
    #meeting
Lunch meeting with Frank Sanchez at noon tomorrow
    remind
    Lunch meeting with Frank Sanchez
    2012-01-02 12:00:00

Remind me to leave the office by 2pm
    remind
    leave the office
    14:00:00

Remember United
    remind
    United


Remember update project waterfall #todo, #"project x"
    remind
    update project waterfall
    
    #todo, #"project x"
Remind me I have a Marketing meeting on 3/14 at 1-2pm
    remind
    I have a Marketing meeting
    2012-03-14 13:00:00 to 2012-03-14 14:00:00

Remember my United mileage number is abcde12345 #travel
    remind
    my United mileage number is abcde12345

    #travel
Remind me about my Yoga class on Tuesday, 8-9am #weekly
    remind
    my Yoga class
    2012-01-03 08:00:00 to 2012-01-03 09:00:00
    #weekly
Remind me to meet sanjay at brinkley's tomorrow at 6 p.m.
    remind
    meet sanjay at brinkley's
    2012-01-02 18:00:00

Remember to meet with nick at brinkley's tomorrow at 6a.m.
    remind
    meet with nick at brinkley's
    2012-01-02 06:00:00

Remember to meet with nick at 7:30am est next Wed
    remind
    meet with nick
    2012-01-04 07:30:00-05:00

Remember JFK to SFO, United #302 Mon 3pm - 5pm EST #travel #"West Coast Sales Conference"
    remind
    JFK to SFO, United #302
    2012-01-02 15:00:00-05:00 to 2012-01-02 17:00:00-05:00
    #travel #"West Coast Sales Conference"
; You need to change Mildred's water tomorrow! Sandy, remind Paul to change Mildred's water on Wednesday evening #weekly.

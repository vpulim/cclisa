var redis = require('redis').createClient();

exports.hook_queue = function(next, connection) {
	var message = connection.transaction.data_lines.join('');
	redis.rpush('inbox', message);
    next(OK);
}

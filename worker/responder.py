from email.mime.text import MIMEText

def respond(command):
    text = 'I GOT THIS:\n\n' + str(command.text)

    msg = MIMEText(text)
    me = command.to
    you = command.sender
    msg['Subject'] = 'Re: ' + command.subject
    msg['From'] = me
    msg['To'] = you

    return msg

from redis import StrictRedis
import smtplib
import responder
import parser

def send_response(message):
    s = smtplib.SMTP('localhost', 2525)
    s.sendmail(message['From'], message['To'], message.as_string())
    s.quit()

def wait_for_command(client):
    ignore, email_content = client.blpop('inbox', 0)
    return parser.parse(email_content)

def run():
    client = StrictRedis()
    while True:
        command = wait_for_command(client)
        response = responder.respond(command)
        send_response(response)

if __name__ == '__main__':
    run()
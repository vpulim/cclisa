from email import message_from_string
import nltk
import nltk.tokenize.regexp
import nltk.chunk
import parsedatetime.parsedatetime as pdt
import re
import datetime
from time import mktime

tagger = nltk.RegexpTagger([
    (r'^remind$', 'VBP'),
    (r'^remember$', 'VB'),
    (r'^reminder$', 'VBN'),
    (r'^to$', 'TO'),
    (r'^\-$', 'DASH'),
    (r'^about$', 'ABOUT'),
    (r'^(from|between)$', 'FROM'),
    (r'^(at|on|by)$', 'ON'),
    (r'^(from|before|after|in a|end of|eod|next|this|last|prev|previous)$', 'NEXT'),
    (r'^#$', 'HASH'),
    (r'^"$', 'QUOT'),
    (r'^[0-5]?[0-9]$', 'DT'),
    (r'^[0-5]?[0-9][ap]m?$', 'DTAM'),
    (r'^[ap]m?$', 'AM'),
    (r'^m$', 'M'),
    (r'^\/$', 'SLASH'),
    (r'^\.$', 'DOT'),
    (r'^:$', ':'),
    (r'^([a-z]{0,2}[ecpmasd]t|utc|gmt)$', 'TZ'),
    (r'^(noon|morning|evening|midnight|night)$', 'TIME'),
    (r'^(mon|tue|wed|thu|fri|sat|sun)$', 'TIME'),
    (r'^(tomorrow|yesterday|today|tonight)$', 'TIME'),
    (r'^(monday|tuesday|wednesday|thursday|friday|saturday|sunday)$', 'TIME'),
    (r'^(january|february|march|april|may|june|july|august|september|october|november|december)$', 'TIME'),
    (r'^(jan|feb|mar|apr|may|jun|jul|aug|sep|oct|nov|dec)$', 'TIME'),
    (r'^20\d\d$', 'TIME'),
    (r'^\d+$', 'NUM'),
    (r'^.*$', 'NN'),
])

tokenizer = nltk.tokenize.regexp.RegexpTokenizer(r"[\w\'\$\%_]+|&|:|#|@|\(|\)|\"|\-|\.|\/")

parser = nltk.RegexpParser(r"""
    TAG: {<HASH><NN>}
    CMD: {(<VBP><NN>*|<VB>)<TO|ABOUT><:>?}
    CMD: {(<VBP><NN>|<VB>)<:>?}
    CMD: {<VBN><:>?}
    TAG: {<HASH><QUOT><.*>+?<QUOT>}
    WHEN: {<NEXT>?<TIME>}
    WHEN: {(<DT><:>)?<DTAM>(<DOT><M><DOT>)?<TZ>?}
    WHEN: {(<DT><:>)?<DT><AM>(<DOT><M><DOT>)?<TZ>?}
    WHEN: {<WHEN|DT|DTAM><TZ>?<TO|DASH|ON><WHEN>}
    WHEN: {<DT><SLASH|DOT|DASH><DT>(<SLASH|DOT|DASH><DT>)?}
    WHEN: {<ON>?<WHEN><WHEN>*}
    WHEN: {<FROM><WHEN>}
""")

commands_re = {
    'remind': re.compile('remind|remember|reminder'),
}

calendar = pdt.Calendar()

tupleType = type(())

class TokenizedText(object):

    def __init__(self, text):
        self.text = text
        self.tokens = tokenizer.tokenize(text.lower())
        # print self.tokens
        self.spans = [span for span in tokenizer.span_tokenize(text.lower())]
        self.i = 0
        self.last = None

    def token_text(self, num_spans):
        text = []
        i_range = range(self.i, min(self.i+num_spans, len(self.spans)))
        for i in i_range:
            span = self.spans[i]
            if self.last:
                text.append(self.text[self.last[1]:span[0]])
            text.append(self.text[span[0]:span[1]])
            self.last = span
            self.i += 1
        return ''.join(text)

class When(object):
    def __init__(self, whenText, sourceTime=None):
        self.date = None
        self.time = None
        self.datetime = None
        self.sourceTime = sourceTime
        self.source = whenText
        self.range = None
        self.text = ''
        self.parse()

    def tupleToDateTime(self, tup):
        return datetime.datetime(*list(tup)[:5])


    def parse(self):
        start, end, ok = calendar.evalRanges(self.source, self.sourceTime)
        if ok:
            self.range = (start, end)
            self.text = ' to '.join([str(start), str(end)])
            return
        dt, kind = calendar.parse(self.source, self.sourceTime)
        if kind == 1: 
            self.date = dt.date()
        elif kind == 2:
            self.time = dt.time()
        elif kind == 3:
            self.datetime = dt
        else:
            return
        self.text = str(self.datetime or self.time or self.date)

    def __repr__(self):
        return self.text

class ParsedText(object):

    def __init__(self, text, sourceTime=None):
        tokenized = TokenizedText(text)
        tagged = tagger.tag(tokenized.tokens)
        tree = parser.parse(tagged)
        # print tree
        what = []
        when = []
        tags = []
        command = []
        for child in tree:
            if type(child) == tupleType:
                what.append(tokenized.token_text(1))
            elif child.node == 'CMD':
                command.append(tokenized.token_text(len(child.flatten())))
            elif child.node == 'WHEN':
                when.append(tokenized.token_text(len(child.flatten())))
            elif child.node == 'TAG':
                tags.append(tokenized.token_text(len(child.flatten())))
        self.what = ''.join(what).strip()
        self.when = When(''.join(when).strip(), sourceTime)
        self.tags = ''.join(tags).strip()
        self.command = 'remind'
        command = ''.join(command).strip()
        for name, pattern in commands_re.items():
            if pattern.match(command):
                self.command = name
                break

    def __repr__(self):
        return '\n'.join([
            'CMD: %s' % self.command,
            'WHT: %s' % self.what,
            'WHN: %s' % self.when,
            'TAG: %s' % self.tags,
            '---'
            ])

class Command(object):

    def __init__(self, sender, to, subject, parts):
        self.sender = sender
        self.to = to
        self.subject = subject
        self.parts = parts
        text = []
        for p in parts:
            if p['type'] == 'text/plain':  
                text.append(p['payload'])
        self.text = ParsedText('\n'.join(text))

def parse(email_string):
    msg = message_from_string(email_string)
    parts = []
    if msg.is_multipart():
        for part in msg.walk():
            if part.get_content_maintype() == 'multipart':
                continue
            parts.append({
                'type': part.get_content_type(),
                'filename': part.get_filename(),
                'payload': part.get_payload(decode=True)
            })
    else:
        parts.append({
            'type': msg.get_content_type(),
            'filename': msg.get_filename(),
            'payload': msg.get_payload(decode=True)
        })
    return Command(msg['From'], msg['To'], msg['Subject'], parts)

if __name__ == '__main__':
    import os
    with open(os.path.dirname(os.path.abspath(__file__)) + '/../test/parser_tests.txt') as f:
        sourceTime = datetime.datetime.strptime(f.readline().strip(), '%Y-%m-%d %H:%M:%S')
        while True:
            line = f.readline()
            if not line: break
            if line[0] == ';' or line[0] == '\n': continue
            line = line.strip()
            command = f.readline().strip()
            what = f.readline().strip()
            when = f.readline().strip()
            tags = f.readline().strip()
            parsed = ParsedText(line, sourceTime)
            bad = []
            print str(parsed)
            if parsed.command != command:
                bad.append((parsed.command, command))
            if parsed.what != what:
                bad.append((parsed.what, what))
            if str(parsed.when) != when:
                bad.append((str(parsed.when), when))
            if parsed.tags != tags:
                bad.append((parsed.tags, tags))
            if len(bad):
                print 'MISMATCH:', line
                print bad

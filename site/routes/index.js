exports.index = function(req, res){
    var events = [
        {
            date: '2/10',
            time: '9:30a',
            description: 'This is a test',
            tags: '#test'
        }
    ];
    res.render('index', { title: 'cc:lisa', events: events });
};